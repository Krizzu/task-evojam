// TO DO --- do this config for development

var ExtractTextPlugin = require('extract-text-webpack-plugin'); //required for extracting

module.exports = {
  entry: ["./src/index.js", './src/stylesheets/mainStyle.scss'],
  module: {
    loaders: [
      {
        test: /\.js?$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
      },
      {
        test: /\.scss$/, // look for .scss files and use loader on them
        exclude: /(node_modules)/,
        loader: ExtractTextPlugin.extract(['css', 'sass']) // extract .scss and use those loaders on that file
      },
    ]
  },
  plugins: [
    // extract to (relative to output path) .css file, and any other chunks ('imports') with it
    new ExtractTextPlugin('/assets/mainStyle.css', {allChunks: true})
  ],
  output: {
    path: __dirname + "/dist/",
    filename: "index.js"
  }
};
