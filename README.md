# My own boilerplate
Small and quick boilerplate project of my own.


## What's included

1. ES6 (BABEL to transpile)
2. SCSS transpiler
- extract text webpack plugin (CSS will be in standalone file)
- Normalize 5.0
3. Webpack + Webpack dev server
- Bundling + live reloading of changes
4. Mocha + Chai - test framework + asser library
5. ESlint - linting with my custom rules

## How to

	1. `git clone` repo, cd into it
	2. `npm install`

`src` folder contains all files, that will be transpiled/bundled/tested.
	- `mainStyle` is the root of all scss styles
	- `index.js` is the root for all JS


## Scripts

**npm start** -- Run devs server at `localhost:8080` with hot reloading (live updating)
**npm run build** -- Build you project - Linting and tests are checked first


## License

MIT.
